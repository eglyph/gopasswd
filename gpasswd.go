// -*- mode: go; tab-width: 2; -*-
package main

import (
	"io/ioutil"
	"flag"
	"log"
	"math/rand"
	"strings"
	"time"
)

func main() {

	wordlist := flag.String("w", "/usr/share/dict/words", "specify wordlist file")
	length := flag.Int("n", 4, "number of words to concatentate together")
	separator := flag.String("s", "-", "symbol to separate words")
	num_passwords := flag.Int("p", 8, "number of passwords to generate")
	leet := flag.Bool("l", true, "substitute letters with similar digits")

	flag.Parse()

	words := ReadWords(wordlist)

	for i := 0; i < *num_passwords; i++ {
		pwd := GeneratePassword(words, length, separator)
		if *leet {
			pwd = LeetSpeak(pwd)
		}

		println(pwd)
	}
}

func ReadWords(wordlist *string) []string {

	words, err := ioutil.ReadFile(*wordlist)
	if err != nil {
		log.Fatal("Cannot open:", *wordlist, err)
	}

	return strings.Split(string(words), "\n")
}

func GeneratePassword(words []string,	length *int, sep *string) string {

	rand.Seed(time.Now().UnixNano())

	password := make([]string, *length)
	count := len(words)

	for used := 0; used < *length; {
		index := rand.Intn(count)
		new_word := string(words[index])
		if len(new_word) > 2 {
			password[used] = new_word
			used++
		}
	}

	return strings.Join(password, *sep)
}

func LeetSpeak(word string) string {
	numbers := []string{"1", "3", "4", "5", "7", "0"}
	letters := []string{"l", "e", "a", "s", "t", "o"}

	for i := range letters {
		letter := letters[i]
		number := numbers[i]
		c := strings.Count(word, letter)
		if c != 0 {
			word = strings.Replace(word, letter, number, rand.Intn(c))
		}
	}
	return word
}

